package com.MAFAvailableHotels.properties;

import java.util.Arrays;
import java.util.List;

public class PropertyConstant {
		
		
		public static String HOTELTYPES_SERVER="BestHotels,CrazyHotels";
		
		public static String HOTELTYPES_ENDPOINTS="BestHotel,CrazyHotels"; 
		
		public static List<String> HOTELSERVERS =Arrays.asList(HOTELTYPES_SERVER.split(","));
		
		public static List<String> HOTELENDPOINTS = Arrays.asList(HOTELTYPES_ENDPOINTS.split(","));
		
		public static final String API_ENDPOINT = "/AvailableHotels";
		
		public static final String HTTP_PREFIX="http://";
		public static final String COLON=":";
		public static final String BACK_SLASH="/";
}
