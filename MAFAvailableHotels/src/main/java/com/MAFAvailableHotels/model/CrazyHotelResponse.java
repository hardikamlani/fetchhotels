package com.MAFAvailableHotels.model;

import java.util.ArrayList;
import java.util.List;

public class CrazyHotelResponse {

	private String hotelName;
	private String hotelRating;
	private Double hotelFare;
	private List<String> amenities;
	private float discount;
	public String getHotelName() {
		return hotelName;
	}
	public void setHotelName(String hotelName) {
		this.hotelName = hotelName;
	}
	public String getHotelRating() {
		return hotelRating;
	}
	public void setHotelRating(String hotelRating) {
		this.hotelRating = hotelRating;
	}
	public Double getHotelFare() {
		return hotelFare;
	}
	public void setHotelFare(Double hotelFare) {
		this.hotelFare = hotelFare;
	}
	public List<String> getAmenities() {
		if(null == amenities){
			amenities = new ArrayList<String>();
		}
		return amenities;
	}
	public void setAmenities(List<String> amenities) {
		this.amenities = amenities;
	}
	public float getDiscount() {
		return discount;
	}
	public void setDiscount(float discount) {
		this.discount = discount;
	}
	
	
}
