package com.MAFAvailableHotels.model;

import java.util.List;

public class AvailableHotelResponse {
	
	private String provider;
	private String hotelName;
	private Double fare;
	private List<String> amenities;
	private Integer hotelRate;
	public String getProvider() {
		return provider;
	}
	public void setProvider(String provider) {
		this.provider = provider;
	}
	public String getHotelName() {
		return hotelName;
	}
	public void setHotelName(String hotelName) {
		this.hotelName = hotelName;
	}
	public Double getFare() {
		return fare;
	}
	public void setFare(Double fare) {
		this.fare = fare;
	}
	public List<String> getAmenities() {
		return amenities;
	}
	public void setAmenities(List<String> amenities) {
		this.amenities = amenities;
	}
	public Integer getHotelRate() {
		return hotelRate;
	}
	public void setHotelRate(Integer hotelRate) {
		this.hotelRate = hotelRate;
	}
	
	
}
