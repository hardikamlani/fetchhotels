package com.MAFAvailableHotels.helper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.function.Consumer;
import java.util.function.Function;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;

import com.MAFAvailableHotels.ExceptionHandler.InvalidRequestObjectParams;
import com.MAFAvailableHotels.model.AvailableHotelResponse;
import com.MAFAvailableHotels.model.BestHotelResponse;
import com.MAFAvailableHotels.model.CrazyHotelResponse;
import com.MAFAvailableHotels.model.HotelRequest;

@Service
public class ServiceHelper {
	
	@Autowired
    private RestTemplate restTemplate;
	
	/**
	 * This method is used to call the bestHotel endpoint and get the respons from it.
	 * @param entity
	 * @param url
	 * @param responseList
	 */
	public void getBestHotelsResponse(HttpEntity<String> entity,String url,List<AvailableHotelResponse> responseList){
		if(null!=entity && url!=null){
			ResponseEntity<BestHotelResponse[]> responseEntity = restTemplate.postForEntity(url,entity, BestHotelResponse[].class);
			BestHotelResponse[] resp = responseEntity.getBody();
			
			List<BestHotelResponse> bestHotelResp = Arrays.asList(resp);
			Consumer<BestHotelResponse> hotelRespConsumer = (BestHotelResponse bstHotelResp)->{
				AvailableHotelResponse hotelObj = new AvailableHotelResponse();
				hotelObj.setProvider("BestHotels");
				hotelObj.setFare(bstHotelResp.getHotelFare());
				hotelObj.setHotelName(bstHotelResp.getHotelName());
				hotelObj.setAmenities(bstHotelResp.getAmenities());
				hotelObj.setHotelRate(bstHotelResp.getHotelRating());
				responseList.add(hotelObj);
			};
			
			bestHotelResp.forEach(hotelRespConsumer);
		}
	}
	/**
	 * This method is used to get the Crazy Hotel service and get the response from it.
	 * @param entity
	 * @param url
	 * @param responseList
	 */
	public void getCrazyHotelsResponse(HttpEntity<String> entity,String url,List<AvailableHotelResponse> responseList){
		if(null!=entity && url!=null){
			ResponseEntity<CrazyHotelResponse[]> responseEntity = restTemplate.postForEntity(url,entity, CrazyHotelResponse[].class);
			CrazyHotelResponse[] resp = responseEntity.getBody();
			
			List<CrazyHotelResponse> crazyHotelResp = Arrays.asList(resp);
			Consumer<CrazyHotelResponse> hotelRespConsumer = (CrazyHotelResponse crzyHotelResp)->{
				AvailableHotelResponse hotelObj = new AvailableHotelResponse();
				hotelObj.setProvider("CrazyHotels");
				hotelObj.setFare(crzyHotelResp.getHotelFare());
				hotelObj.setHotelName(crzyHotelResp.getHotelName());
				hotelObj.setAmenities(crzyHotelResp.getAmenities());
				hotelObj.setHotelRate(StringUtils.countOccurrencesOf(crzyHotelResp.getHotelRating(), "*"));
				responseList.add(hotelObj);
			};
			
			crazyHotelResp.forEach(hotelRespConsumer);
		}
		
	}
	
	/**
	 * This method is used to validate the request Object.
	 * Hardik
	 * @param requestObject
	 * @return
	 * @throws InvalidRequestObjectParams
	 */
	public boolean validateRequestObject(HotelRequest requestObject) throws InvalidRequestObjectParams{
		boolean returnValue = false;
		Date frmDate = null;
		Date toDate = null;
		Date dt = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		try{
		if(null!=requestObject){
			
			
			frmDate = sdf.parse(requestObject.getFromDate());
            if (!requestObject.getFromDate().equals(sdf.format(frmDate))) {
            	frmDate = null;
            }else if(frmDate.getTime() <= dt.getTime()){
            	throw new InvalidRequestObjectParams();
            }// if it does not goes inside the if block , it means date is valid.
            toDate = sdf.parse(requestObject.getToDate());
            if(!requestObject.getToDate().equals(sdf.format(toDate))){
            	toDate = null;
            }else if(toDate.getTime() <= dt.getTime()){
            	throw new InvalidRequestObjectParams();
            }
            
            if(toDate.getTime() < frmDate.getTime()){
            	throw new InvalidRequestObjectParams();
            }
            
            if( (requestObject.getNumberOfAdults() <0)){
            	throw new InvalidRequestObjectParams();
            }
            
            if(requestObject.getCity().equals("")){
            	throw new InvalidRequestObjectParams();
            }
			returnValue = true;
		}
		
		}catch(ParseException e){
			throw new InvalidRequestObjectParams();
		}
		
		return returnValue;
	}
	
	public void formatDatetoISOInstant(HotelRequest hotelReq,String apiName){
		
		if(apiName.equals("CrazyHotels")){
			Instant bt = Instant.now();
			DateTimeFormatter f = DateTimeFormatter.ISO_INSTANT;
			String dtValue = f.format(bt);
			hotelReq.setFromDate(hotelReq.getFromDate()+dtValue.substring(dtValue.indexOf("T")) );
			hotelReq.setToDate(hotelReq.getToDate()+dtValue.substring(dtValue.indexOf("T")) );
		}
		
	}
	
}
