package com.MAFAvailableHotels.controller;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.MAFAvailableHotels.ExceptionHandler.ErrorDetails;
import com.MAFAvailableHotels.ExceptionHandler.InvalidRequestObjectParams;
import com.MAFAvailableHotels.helper.ServiceHelper;
import com.MAFAvailableHotels.model.AvailableHotelResponse;
import com.MAFAvailableHotels.model.HotelRequest;
import com.MAFAvailableHotels.model.HotelResponse;
import com.MAFAvailableHotels.properties.PropertyConstant;

import com.fasterxml.jackson.databind.ObjectMapper;



@RestController
@RequestMapping(value="/AvailableHotels")
public class HotelController {
	
	@Autowired
	DiscoveryClient client;
	
	@Autowired
	ServiceHelper serviceHelper;
	
	/**
	 * This method will act an controller to get the list of available hotels from the Crazy and Best hotels.
	 * @param hotelObj
	 * @return
	 */
	@PostMapping
	@ResponseBody
	public ResponseEntity<?> getAvaibleHotels(@RequestBody HotelRequest hotelObj){
		List<AvailableHotelResponse> responseList = new ArrayList<AvailableHotelResponse>();
		List<HotelResponse> response = new ArrayList<HotelResponse>();
		try{
			Consumer<List<AvailableHotelResponse>> addtoList = (List<AvailableHotelResponse> inputList)->{
				responseList.addAll(inputList);
			};
		
			Function<String,List<AvailableHotelResponse>> callSeverFn = (String inputString) -> {

				int indexOf = PropertyConstant.HOTELSERVERS.indexOf(inputString);
				String endpointName = PropertyConstant.HOTELENDPOINTS.get(indexOf);
				serviceHelper.formatDatetoISOInstant(hotelObj, inputString);
				List<AvailableHotelResponse> sbList = this.getHotelResponse(hotelObj, inputString, endpointName);
				return sbList;
			};
		
			Consumer<AvailableHotelResponse> mapAvailtoHotelResp = (AvailableHotelResponse availHotel) -> {
				HotelResponse htlResp = new HotelResponse();
				BeanUtils.copyProperties(availHotel, htlResp);
				htlResp.setAmenities(htlResp.getAmenities());
				response.add(htlResp);
			};
			if(null!=hotelObj && serviceHelper.validateRequestObject(hotelObj)){
				PropertyConstant.HOTELSERVERS.parallelStream().map(callSeverFn).forEach(addtoList);
			}

			responseList.stream().sorted(Comparator.comparing(AvailableHotelResponse::getHotelRate))
					.forEach(mapAvailtoHotelResp);
		}catch (InvalidRequestObjectParams e) {
			ErrorDetails errorDetails = new ErrorDetails(new Date(), "Something is wrong with the request",
					"Something is wrong with the request");
			return new ResponseEntity<>(errorDetails, HttpStatus.BAD_REQUEST);
		} catch (Exception e) {
			ErrorDetails errorDetails = new ErrorDetails(new Date(), "Exception Occured while processing your request",
					"Exception Occured while processing your request");
			return new ResponseEntity<>(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<>(response, HttpStatus.CREATED);
	}
	/**
	 * This method is used to make the service call to other service to get the response from them
	 * @param hotelReq
	 * @param instanceName
	 * @param apiName
	 * @return
	 */
	public List<AvailableHotelResponse> getHotelResponse(HotelRequest hotelReq,String instanceName, String apiName){
		List<AvailableHotelResponse> responseList = new ArrayList<AvailableHotelResponse>();
		
		String reqString;
		List<ServiceInstance> application = client.getInstances(instanceName);
		String url = PropertyConstant.HTTP_PREFIX  + application.get(0).getHost() + PropertyConstant.COLON + application.get(0).getPort() + PropertyConstant.BACK_SLASH + apiName;

        try{
        	ObjectMapper mapper = new ObjectMapper();
        	reqString = mapper.writeValueAsString(hotelReq);
        	HttpHeaders headers = new HttpHeaders();
        	headers.setContentType(MediaType.APPLICATION_JSON);
        	HttpEntity<String> entity = new HttpEntity<String>(reqString,headers);
        	
        	if(apiName.equals("BestHotel")){
        		serviceHelper.getBestHotelsResponse(entity, url, responseList);
        	}else if(apiName.equals("CrazyHotels")){
        		serviceHelper.getCrazyHotelsResponse(entity, url, responseList);
        	}
        
        }catch(Exception e){
        	return responseList;
        } 
       
        return responseList;
	}

}
