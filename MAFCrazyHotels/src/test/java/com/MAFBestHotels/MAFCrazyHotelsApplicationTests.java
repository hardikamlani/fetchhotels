package com.MAFBestHotels;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import com.MAFCrazyHotels.MAFCrazyHotelsApplication;
import com.MAFCrazyHotels.ExceptionHandler.ErrorDetails;
import com.MAFCrazyHotels.entity.HotelResponse;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.response.Response;
import com.jayway.restassured.specification.RequestSpecification;

@RunWith(SpringRunner.class)
@SpringBootTest(classes=MAFCrazyHotelsApplication.class,webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT) 
@TestPropertySource(value={"classpath:application.properties"})
public class MAFCrazyHotelsApplicationTests {
	
	@LocalServerPort
	private int port;
	
	@Before
    public void setBaseUri () {
            RestAssured.port = port;
            RestAssured.baseURI = "http://localhost"; // replace as appropriate
    }

	@Test
	public void getTheCrazyHotelsResponsePositive() {
		RequestSpecification httpRequest = RestAssured.given();
        httpRequest.header("Content-Type", "application/json");
        Gson gson = new Gson();
        JsonObject jsonObject = new JsonObject();
        jsonObject = this.prepareRequestObject("2019-01-12", "2019-01-20", "auh", 1);
        httpRequest.body(jsonObject.toString());
        Response response = httpRequest.post("/CrazyHotels");
        
        if(response.statusCode()==200 || response.statusCode()==201){
        	String respBody =  response.getBody().asString();
        	HotelResponse[] arr_ofHotels = gson.fromJson(respBody, HotelResponse[].class);
        	Assert.assertEquals(2, arr_ofHotels.length);
        	
        }else{
        	Assert.fail("Unexpected Response from the Test case :: "+ "getTheCrazyHotelsResponsePositive()" );
        }
	}
	
	@Test
	public void getTheCrazyHotelsZeroResults() {
		RequestSpecification httpRequest = RestAssured.given();
        httpRequest.header("Content-Type", "application/json");
        Gson gson = new Gson();
        JsonObject jsonObject = new JsonObject();
        jsonObject = this.prepareRequestObject("2020-01-12", "2020-01-20", "auh", 1);
        httpRequest.body(jsonObject.toString());
        Response response = httpRequest.post("/CrazyHotels");
        
        if(response.statusCode()==200 || response.statusCode()==201){
        	String respBody =  response.getBody().asString();
        	HotelResponse[] arr_ofHotels = gson.fromJson(respBody, HotelResponse[].class);
        	Assert.assertEquals(0, arr_ofHotels.length);
        	
        }else{
        	Assert.fail("Unexpected Response from the Test case :: "+ "getTheCrazyHotelsZeroResults()" );
        }
	}
	
	@Test
	public void getTheCrazyHotelsInvalidRequest() {
		RequestSpecification httpRequest = RestAssured.given();
        httpRequest.header("Content-Type", "application/json");
        Gson gson = new Gson();
        httpRequest.body("Hello World");
        Response response = httpRequest.post("/CrazyHotels");
        
        if(response.statusCode()==400 ){
        	String respBody =  response.getBody().asString();
        	ErrorDetails arr_ofHotels = gson.fromJson(respBody, ErrorDetails.class);
        	Assert.assertEquals(true, arr_ofHotels.getMessage().contains("Request is MalFormed"));
        	
        }else{
        	Assert.fail("Unexpected Response from the Test case :: "+ "getTheCrazyHotelsInvalidRequest()" );
        }
	}
	
	@Test
	public void getTheCrazyHotelsInvalidFromDateRequest() {
		RequestSpecification httpRequest = RestAssured.given();
        httpRequest.header("Content-Type", "application/json");
        Gson gson = new Gson();
        JsonObject jsonObject = new JsonObject();
        jsonObject = this.prepareRequestObject("202012", "2020-01-20", "auh", 1);
        httpRequest.body(jsonObject.toString());
        Response response = httpRequest.post("/CrazyHotels");
        
        if(response.statusCode()==400 ){
        	String respBody =  response.getBody().asString();
        	ErrorDetails arr_ofHotels = gson.fromJson(respBody, ErrorDetails.class);
        	Assert.assertEquals(true, arr_ofHotels.getMessage().contains("wrong with the request"));
        	
        }else{
        	Assert.fail("Unexpected Response from the Test case :: "+ "getTheCrazyHotelsInvalidFromDateRequest()" );
        }
	}
	
	@Test
	public void getTheCrazyHotelsInvalidToDateRequest() {
		RequestSpecification httpRequest = RestAssured.given();
        httpRequest.header("Content-Type", "application/json");
        Gson gson = new Gson();
        JsonObject jsonObject = new JsonObject();
        jsonObject = this.prepareRequestObject("2019-01-12", "202020", "auh", 1);
        httpRequest.body(jsonObject.toString());
        Response response = httpRequest.post("/CrazyHotels");
        
        if(response.statusCode()==400 ){
        	String respBody =  response.getBody().asString();
        	ErrorDetails arr_ofHotels = gson.fromJson(respBody, ErrorDetails.class);
        	Assert.assertEquals(true, arr_ofHotels.getMessage().contains("wrong with the request"));
        	
        }else{
        	Assert.fail("Unexpected Response from the Test case :: "+ "getTheCrazyHotelsInvalidFromDateRequest()" );
        }
	}
	
	@Test
	public void getTheCrazyHotelsTolessthanFromDateRequest() {
		RequestSpecification httpRequest = RestAssured.given();
        httpRequest.header("Content-Type", "application/json");
        Gson gson = new Gson();
        JsonObject jsonObject = new JsonObject();
        jsonObject = this.prepareRequestObject("2019-01-12", "2018-01-20", "auh", 1);
        httpRequest.body(jsonObject.toString());
        Response response = httpRequest.post("/CrazyHotels");
        
        if(response.statusCode()==400 ){
        	String respBody =  response.getBody().asString();
        	ErrorDetails arr_ofHotels = gson.fromJson(respBody, ErrorDetails.class);
        	Assert.assertEquals(true, arr_ofHotels.getMessage().contains("wrong with the request"));
        	
        }else{
        	Assert.fail("Unexpected Response from the Test case :: "+ "getTheCrazyHotelsInvalidFromDateRequest()" );
        }
	}
	
	@Test
	public void getTheCrazyHotelsInvalidNumofAdults() {
		RequestSpecification httpRequest = RestAssured.given();
        httpRequest.header("Content-Type", "application/json");
        Gson gson = new Gson();
        JsonObject jsonObject = new JsonObject();
        jsonObject = this.prepareRequestObject("2019-01-12", "2019-01-20", "auh", -3);
        httpRequest.body(jsonObject.toString());
        Response response = httpRequest.post("/CrazyHotels");
        
        if(response.statusCode()==400 ){
        	String respBody =  response.getBody().asString();
        	ErrorDetails arr_ofHotels = gson.fromJson(respBody, ErrorDetails.class);
        	Assert.assertEquals(true, arr_ofHotels.getMessage().contains("wrong with the request"));
        	
        }else{
        	Assert.fail("Unexpected Response from the Test case :: "+ "getTheCrazyHotelsInvalidFromDateRequest()" );
        }
	}
	
	@Test
	public void getTheCrazyHotelsInvalidCity() {
		RequestSpecification httpRequest = RestAssured.given();
        httpRequest.header("Content-Type", "application/json");
        Gson gson = new Gson();
        JsonObject jsonObject = new JsonObject();
        jsonObject = this.prepareRequestObject("2019-01-12", "2019-01-20", "yui", 3);
        httpRequest.body(jsonObject.toString());
        Response response = httpRequest.post("/CrazyHotels");
        
        if(response.statusCode()==400 ){
        	String respBody =  response.getBody().asString();
        	ErrorDetails arr_ofHotels = gson.fromJson(respBody, ErrorDetails.class);
        	Assert.assertEquals(true, arr_ofHotels.getMessage().contains("wrong with the request"));
        	
        }else if(response.statusCode()==200 || response.statusCode()==201){
        	String respBody =  response.getBody().asString();
        	HotelResponse[] arr_ofHotels = gson.fromJson(respBody, HotelResponse[].class);
        	Assert.assertEquals(0, arr_ofHotels.length);
        	
        }else{
        	Assert.fail("Unexpected Response from the Test case :: "+ "getTheCrazyHotelsInvalidFromDateRequest()" );
        }
	}
	
	
	public JsonObject prepareRequestObject(String fromDate,String toDate,String city,int numofAdults){
		
		 	JsonObject jsonObject = new JsonObject();
		 	jsonObject.addProperty("fromDate", fromDate);
	        jsonObject.addProperty("toDate", toDate);
	        jsonObject.addProperty("city", city);
	        jsonObject.addProperty("numberOfAdults", numofAdults);
	        return jsonObject;
	}

}

