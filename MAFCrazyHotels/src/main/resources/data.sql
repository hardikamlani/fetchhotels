insert into hotels (
       id ,
        adult_count ,
        amenities ,
        city ,
        from_date ,
        hotel_fare ,
        hotel_name,
        hotel_rating ,
        provider_name ,
        to_date ,
        discount
    )
values(10909,10,'Shower_Bath','AUH','2018-01-01','20','Avaya1',5,'BestHotels','2018-12-12',0);

insert into hotels (
       id ,
        adult_count ,
        amenities ,
        city ,
        from_date ,
        hotel_fare ,
        hotel_name,
        hotel_rating ,
        provider_name ,
        to_date ,
        discount
    )
values(10910,10,'Shower_Bath','AUH','2019-01-01','20','Avaya2',5,'BestHotels','2019-12-12',0);

insert into hotels (
       id ,
        adult_count ,
        amenities ,
        city ,
        from_date ,
        hotel_fare ,
        hotel_name,
        hotel_rating ,
        provider_name ,
        to_date ,
        discount
    )
values(10913,10,'Shower_Bath','AUH','2018-01-01','20','Avaya3',5,'BestHotels','2018-12-12',10);

insert into hotels (
       id ,
        adult_count ,
        amenities ,
        city ,
        from_date ,
        hotel_fare ,
        hotel_name,
        hotel_rating ,
        provider_name ,
        to_date ,
        discount
    )
values(10911,10,'Shower_Bath','AUH','2018-01-01','20','Avaya4',5,'BestHotels','2018-12-12',15);

insert into hotels (
       id ,
        adult_count ,
        amenities ,
        city ,
        from_date ,
        hotel_fare ,
        hotel_name,
        hotel_rating ,
        provider_name ,
        to_date ,
        discount
    )
values(10912,10,'Shower_Bath','AUH','2019-01-01','20','Avaya5',4,'BestHotels','2019-12-12',5);