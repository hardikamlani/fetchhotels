package com.MAFCrazyHotels.service;

import java.util.List;

import com.MAFCrazyHotels.ExceptionHandler.InvalidRequestObjectParams;
import com.MAFCrazyHotels.entity.HotelRequest;
import com.MAFCrazyHotels.entity.HotelResponse;


public interface MAFHotelsService {
	
		public List<HotelResponse> getListofHotels(HotelRequest requestObject) throws InvalidRequestObjectParams;
	
}
