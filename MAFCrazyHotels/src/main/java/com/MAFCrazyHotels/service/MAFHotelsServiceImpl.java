package com.MAFCrazyHotels.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.MAFCrazyHotels.ExceptionHandler.InvalidRequestObjectParams;
import com.MAFCrazyHotels.entity.Hotel;
import com.MAFCrazyHotels.entity.HotelRequest;
import com.MAFCrazyHotels.entity.HotelResponse;
import com.MAFCrazyHotels.repository.MAFHotelsCustomRepository;
import com.MAFCrazyHotels.utility.CommonUtility;

@Service(value="hotelservice")
public class MAFHotelsServiceImpl implements MAFHotelsService {
	
	@Autowired
	MAFHotelsCustomRepository customRepo;

	/**
	 * This method will validate the request object and will send error incase  of probleam
	 * else it will call repository and would give back response in case of success
	 * Hardik
	 */
	@Override
	public List<HotelResponse> getListofHotels(HotelRequest requestObject) throws InvalidRequestObjectParams {
		
		List<Hotel> responseObject = null;
		List<HotelResponse> respObject =new ArrayList<HotelResponse>();
		if(null!=requestObject && this.validateRequestObject(requestObject)){
			responseObject = customRepo.getListofHotels(requestObject);
		}
		
		Function<Integer,String> convertRatetoString = (Integer inputValue)->{
			String returnValue;
			returnValue = String.join("", Collections.nCopies(inputValue, "*"));
			return returnValue;
		};
		
		Function<String,Double> convertStringToDouble = (String inputValue) -> {
			Double returnValue;
			BigDecimal bdValue =  new BigDecimal(inputValue);
			returnValue = bdValue.setScale(2,BigDecimal.ROUND_HALF_UP).doubleValue();
			return returnValue;
		};
		
		Consumer<Hotel> copyDataHotel = (Hotel hotelObj) -> {
			HotelResponse hotelResponse = new HotelResponse();
			BeanUtils.copyProperties(hotelObj, hotelResponse);
			hotelResponse.setAmenities(Arrays.asList(hotelObj.getAmenities().split("_")));
			hotelResponse.setHotelFare(convertStringToDouble.apply(hotelObj.getHotelFare()));
			hotelResponse.setHotelRating(convertRatetoString.apply(hotelObj.getHotelRating()));
			respObject.add(hotelResponse);
		};
		
		if(null!=responseObject){
			responseObject.forEach(copyDataHotel);
		}
		// TODO Auto-generated method stub
		return respObject;
	}
	
	/**
	 * This method is used to validate the request Object.
	 * Hardik
	 * @param requestObject
	 * @return
	 * @throws InvalidRequestObjectParams
	 */
	public boolean validateRequestObject(HotelRequest requestObject) throws InvalidRequestObjectParams {
		boolean returnValue = false;
		Date frmDate = null;
		Date toDate = null;
		Date dt = new Date();
		try {
			if (null != requestObject) {

				frmDate = CommonUtility.getFormattedDate(requestObject.getFromDate());
				if ((null == frmDate) || (frmDate.getTime() <= dt.getTime())) {
					throw new InvalidRequestObjectParams();
				} // if it does not goes inside the if block , it means date is
					// valid.
				toDate = CommonUtility.getFormattedDate(requestObject.getToDate());
				if ((null == toDate) || (toDate.getTime() <= dt.getTime())) {
					throw new InvalidRequestObjectParams();
				} // if it does not goes inside the if block , it means date is
					// valid.

				if (toDate.getTime() < frmDate.getTime()) {
					throw new InvalidRequestObjectParams();
				} // it means that to date is above from Date

				if ((requestObject.getNumberOfAdults() < 0)) {
					throw new InvalidRequestObjectParams();
				} // it means no of adults is less than 0

				if (requestObject.getCity().equals("")) {
					throw new InvalidRequestObjectParams();
				} // it means that the city is blank.
				returnValue = true;
			}

		} catch (Exception e) {
			throw new InvalidRequestObjectParams();
		}

		return returnValue;
	}

}
