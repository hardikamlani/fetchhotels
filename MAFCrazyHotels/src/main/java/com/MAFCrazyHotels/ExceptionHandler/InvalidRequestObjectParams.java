package com.MAFCrazyHotels.ExceptionHandler;

/**
 * 
 * This method is used to act as the invalid request param class.
 * @author Hardik
 *
 */
public class InvalidRequestObjectParams extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
