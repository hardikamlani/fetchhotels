package com.MAFCrazyHotels.entity;
/**
 * 
 * This Class is used as a RequestObject to get the Hotels by criteria
 * @author Hardik
 *
 */
public class HotelRequest {

	private String fromDate;
	private String toDate;
	private String city;
	private int numberOfAdults;
	public String getFromDate() {
		return fromDate;
	}
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}
	public String getToDate() {
		return toDate;
	}
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public int getNumberOfAdults() {
		return numberOfAdults;
	}
	public void setNumberOfAdults(int numberOfAdults) {
		this.numberOfAdults = numberOfAdults;
	}
	
	
}
