package com.MAFCrazyHotels.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@NamedQuery(name = "Hotel.getAvailableHotelsbyInput",
query = "SELECT h FROM Hotel h WHERE LOWER(h.city) =?1 and h.adultCount >=?2 and h.toDate>= ?4 and h.fromDate<=?3"
)
@Entity
@Table(name = "Hotels")
public class Hotel {
	
	@Id
	@GeneratedValue
	private Long id;
	
	@NotNull @NotEmpty
	private String providerName;
	
	@NotNull @Temporal(TemporalType.DATE)
	private Date fromDate;
	
	@NotNull @Temporal(TemporalType.DATE)
	private Date toDate ;
	
	@NotNull @NotEmpty 
	private String city;
	
	private Integer adultCount;
	
	@NotNull @NotEmpty 
	private String hotelName;
	
	@NotNull @NotEmpty @Min(value=1) @Max(value=5)
	private Integer hotelRating;
	
	@NotNull @NotEmpty 
	private String hotelFare;
	
	@NotNull @NotEmpty 
	private String amenities;
	
	private float discount;

	public String getProviderName() {
		return providerName;
	}

	public void setProviderName(String providerName) {
		this.providerName = providerName;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public int getAdultCount() {
		return adultCount;
	}

	public void setAdultCount(int adultCount) {
		this.adultCount = adultCount;
	}

	public String getHotelName() {
		return hotelName;
	}

	public void setHotelName(String hotelName) {
		this.hotelName = hotelName;
	}

	public Integer getHotelRating() {
		return hotelRating;
	}

	public void setHotelRating(Integer hotelRating) {
		this.hotelRating = hotelRating;
	}

	public String getHotelFare() {
		return hotelFare;
	}

	public void setHotelFare(String hotelFare) {
		this.hotelFare = hotelFare;
	}

	public String getAmenities() {
		return amenities;
	}

	public void setAmenities(String amenities) {
		this.amenities = amenities;
	}

	public float getDiscount() {
		return discount;
	}

	public void setDiscount(float discount) {
		this.discount = discount;
	}
	
	
	
}
