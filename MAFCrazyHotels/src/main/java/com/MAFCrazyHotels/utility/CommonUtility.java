package com.MAFCrazyHotels.utility;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class CommonUtility {
	
	/**
	 * This method is used to parse the date in the required format
	 * @param inputDate
	 * @return
	 */
	public static Date getFormattedDate(String inputDate){
		Date returnValue=null;
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
			sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
			returnValue = sdf.parse(inputDate);
		} catch (Exception e) {
			e.printStackTrace();
			returnValue = null;
		}
		
		return returnValue;
	}

}
