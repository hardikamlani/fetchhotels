package com.MAFCrazyHotels;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages="com.MAFCrazyHotels")
@EnableDiscoveryClient
@EnableEurekaClient
public class MAFCrazyHotelsApplication {

	public static void main(String ...args) {
		SpringApplication.run(MAFCrazyHotelsApplication.class, args);
	}

}

