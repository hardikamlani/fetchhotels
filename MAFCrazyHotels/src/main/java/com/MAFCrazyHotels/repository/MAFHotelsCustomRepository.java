package com.MAFCrazyHotels.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.MAFCrazyHotels.entity.Hotel;
import com.MAFCrazyHotels.entity.HotelRequest;
import com.MAFCrazyHotels.utility.CommonUtility;

@Repository(value="customRepo")
public class MAFHotelsCustomRepository {
	
	@PersistenceContext
    EntityManager entityManager;

	/**
	 * This method is used to hit the DB to retrieve the details from the DB.
	 * Hardik
	 * @param requestObject
	 * @return
	 */
	public List<Hotel> getListofHotels(HotelRequest requestObject){
		
		List<Hotel> respnse = null;
		
		Query hotelQuery = entityManager.createNamedQuery("Hotel.getAvailableHotelsbyInput");
		if(null!=requestObject){
		hotelQuery.setParameter(1, requestObject.getCity().toLowerCase());
		hotelQuery.setParameter(2, requestObject.getNumberOfAdults());
		hotelQuery.setParameter(3, CommonUtility.getFormattedDate(requestObject.getFromDate()));
		hotelQuery.setParameter(4, CommonUtility.getFormattedDate(requestObject.getToDate()));
		respnse = (List<Hotel>)hotelQuery.getResultList();
		}
		return respnse;
	}
}
