package com.MAFCrazyHotels.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.MAFCrazyHotels.ExceptionHandler.ErrorDetails;
import com.MAFCrazyHotels.ExceptionHandler.InvalidRequestObjectParams;
import com.MAFCrazyHotels.entity.HotelRequest;
import com.MAFCrazyHotels.entity.HotelResponse;
import com.MAFCrazyHotels.service.MAFHotelsService; 

@RestController
@RequestMapping(value= "/CrazyHotels")
public class MAFHotelsController {
	
	@Autowired
	MAFHotelsService hotelservice;
	
	/**
	 * This method is used to retrieve the CrazyHotels and it will act as a Post endpoint.
	 * Hardik
	 * @param hotelReq
	 * @return
	 */
	@RequestMapping(method=RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<?> retrieveHotels(@RequestBody HotelRequest hotelReq) {
		List<HotelResponse> responseList =null;
		
		try{
			
			if(null!=hotelReq){
				responseList = hotelservice.getListofHotels(hotelReq);
			}else{
				responseList = new ArrayList<HotelResponse>();
			}
		}catch(InvalidRequestObjectParams e){
			ErrorDetails errorDetails = new ErrorDetails(new Date(), "Something is wrong with the request",
					"Something is wrong with the request");
			return new ResponseEntity<>(errorDetails, HttpStatus.BAD_REQUEST);
		}catch(Exception e){
			ErrorDetails errorDetails = new ErrorDetails(new Date(), "Exception Occured while processing your request",
					"Exception Occured while processing your request");
			return new ResponseEntity<>(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		return new ResponseEntity<>(responseList,HttpStatus.CREATED);
	} 

}
