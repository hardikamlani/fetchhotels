insert into hotels (
       id ,
        adult_count ,
        amenities ,
        city ,
        from_date ,
        hotel_fare ,
        hotel_name,
        hotel_rating ,
        provider_name ,
        to_date 
    )
values(10909,10,'Shower_Bath','AUH','2019-01-01','20','Avaya1',5,'BestHotels','2019-12-12');

insert into hotels (
       id ,
        adult_count ,
        amenities ,
        city ,
        from_date ,
        hotel_fare ,
        hotel_name,
        hotel_rating ,
        provider_name ,
        to_date 
    )
values(10910,10,'Shower_Bath','AUH','2018-01-01','20','Avaya2',5,'BestHotels','2018-12-12');

insert into hotels (
       id ,
        adult_count ,
        amenities ,
        city ,
        from_date ,
        hotel_fare ,
        hotel_name,
        hotel_rating ,
        provider_name ,
        to_date 
    )
values(10913,10,'Shower_Bath','AUH','2019-01-01','20','Avaya3',1,'BestHotels','2019-12-12');

insert into hotels (
       id ,
        adult_count ,
        amenities ,
        city ,
        from_date ,
        hotel_fare ,
        hotel_name,
        hotel_rating ,
        provider_name ,
        to_date 
    )
values(10911,10,'Shower_Bath','AUH','2019-01-01','20.45687','Avaya4',2,'BestHotels','2019-12-12');

insert into hotels (
       id ,
        adult_count ,
        amenities ,
        city ,
        from_date ,
        hotel_fare ,
        hotel_name,
        hotel_rating ,
        provider_name ,
        to_date 
    )
values(10912,10,'Shower_Bath','AUH','2018-01-01','20.8907','Avaya5',5,'BestHotels','2018-12-12');