package com.MAFBestHotels.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.MAFBestHotels.entity.Hotel;
import com.MAFBestHotels.entity.HotelRequest;
import com.MAFBestHotels.utility.CommonUtility;

@Repository(value="customRepo")
public class MAFHotelsCustomRepository {
	
	@PersistenceContext
    EntityManager entityManager;
	
	/**
	 * This method is used to get the best hotels from the database by input configuration
	 * based by namequery
	 * @param requestObject
	 * @return
	 */
	public List<Hotel> getListofHotels(HotelRequest requestObject){
		
		List<Hotel> respnse = null;
		
		Query hotelQuery = entityManager.createNamedQuery("Hotel.getAvailableHotelsbyInput");
		if(null!=requestObject){
		hotelQuery.setParameter(1, requestObject.getCity().toLowerCase());
		hotelQuery.setParameter(2, requestObject.getNumberOfAdults());
		hotelQuery.setParameter(3, CommonUtility.getFormattedDate(requestObject.getFromDate()));
		hotelQuery.setParameter(4, CommonUtility.getFormattedDate(requestObject.getToDate()));
		respnse = (List<Hotel>)hotelQuery.getResultList();
		}
		return respnse;
	}
}
