package com.MAFBestHotels.service;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.MAFBestHotels.ExceptionHandler.InvalidRequestObjectParams;
import com.MAFBestHotels.entity.Hotel;
import com.MAFBestHotels.entity.HotelRequest;
import com.MAFBestHotels.entity.HotelResponse;
import com.MAFBestHotels.repository.MAFHotelsCustomRepository;
import com.MAFBestHotels.utility.CommonConstants;


@Service(value="hotelservice")
public class MAFHotelsServiceImpl implements MAFHotelsService {
	
	@Autowired
	MAFHotelsCustomRepository customRepo;
	
	/**
	 * This method is ued to get the list of hotels from the DB , it will act an service . 
	 */
	@Override
	public List<HotelResponse> getListofHotels(HotelRequest requestObject) throws InvalidRequestObjectParams {
		
		List<Hotel> responseObject = null;
		List<HotelResponse> respObject =new ArrayList<HotelResponse>();
		if(null!=requestObject && this.validateRequestObject(requestObject)){
			responseObject = customRepo.getListofHotels(requestObject);
		}
		
		Function<String,Double> convertStringToDouble = (String inputValue) -> {
			Double returnValue;
			BigDecimal bdValue =  new BigDecimal(inputValue);
			returnValue = bdValue.setScale(2,BigDecimal.ROUND_HALF_UP).doubleValue();
			return returnValue;
		};
		
		Consumer<Hotel> copyDataHotel = (Hotel hotelObj) -> {
			HotelResponse hotelResponse = new HotelResponse();
			BeanUtils.copyProperties(hotelObj, hotelResponse);
			hotelResponse.setAmenities(Arrays.asList(hotelObj.getAmenities().split("_")));
			hotelResponse.setHotelFare(convertStringToDouble.apply(hotelObj.getHotelFare()));
			respObject.add(hotelResponse);
		};
		
		if(null!=responseObject){
			responseObject.forEach(copyDataHotel);
		}
		return respObject;
	}
	
	/**
	 * This method is used to validate the request Object.
	 * Hardik
	 * @param requestObject
	 * @return
	 * @throws InvalidRequestObjectParams
	 */
	public boolean validateRequestObject(HotelRequest requestObject) throws InvalidRequestObjectParams{
		boolean returnValue = false;
		Date frmDate = null;
		Date toDate = null;
		Date dt = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat(CommonConstants.INPUTREQ_DATEFORMAT);
		try{
		if(null!=requestObject){
			
			
			frmDate = sdf.parse(requestObject.getFromDate());
            if (!requestObject.getFromDate().equals(sdf.format(frmDate))) {
            	frmDate = null;
            }else if(frmDate.getTime() <= dt.getTime()){
            	throw new InvalidRequestObjectParams();
            }// if it does not goes inside the if block , it means date is valid.
            toDate = sdf.parse(requestObject.getToDate());
            if(!requestObject.getToDate().equals(sdf.format(toDate))){
            	toDate = null;
            }else if(toDate.getTime() <= dt.getTime()){
            	throw new InvalidRequestObjectParams();
            }
            
            if(toDate.getTime() < frmDate.getTime()){
            	throw new InvalidRequestObjectParams();
            }
            
            if( (requestObject.getNumberOfAdults() <0)){
            	throw new InvalidRequestObjectParams();
            }
            
            if(requestObject.getCity().equals("")){
            	throw new InvalidRequestObjectParams();
            }
			returnValue = true;
		}
		
		}catch(ParseException e){
			throw new InvalidRequestObjectParams();
		}
		
		return returnValue;
	}

}
