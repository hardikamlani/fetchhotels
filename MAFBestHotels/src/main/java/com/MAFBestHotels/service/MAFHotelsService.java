package com.MAFBestHotels.service;

import java.util.List;

import com.MAFBestHotels.ExceptionHandler.InvalidRequestObjectParams;
import com.MAFBestHotels.entity.HotelRequest;
import com.MAFBestHotels.entity.HotelResponse;


public interface MAFHotelsService {
	
		public List<HotelResponse> getListofHotels(HotelRequest requestObject) throws InvalidRequestObjectParams;
	
}
