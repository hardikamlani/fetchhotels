package com.MAFBestHotels.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.MAFBestHotels.ExceptionHandler.ErrorDetails;
import com.MAFBestHotels.ExceptionHandler.InvalidRequestObjectParams;
import com.MAFBestHotels.entity.HotelRequest;
import com.MAFBestHotels.entity.HotelResponse;
import com.MAFBestHotels.service.MAFHotelsService;
import java.util.logging.Logger;

@RestController
@RequestMapping(value = "/BestHotel")
public class MAFHotelsController {
	
	private final static Logger LOGGER = Logger.getLogger(MAFHotelsController.class.getName());

	@Autowired
	MAFHotelsService hotelservice;

	/**
	 * This method is used to retrieve the best hotels fromt the DB by input 
	 * it will act as a controller for the frontend service to integrate
	 * @param inputReq
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<?> retrieveHotels(@RequestBody HotelRequest inputReq) {
		List<HotelResponse> responseList = null;
		try {
			if (null != inputReq) {
				responseList = hotelservice.getListofHotels(inputReq);
			} else {
				responseList = new ArrayList<HotelResponse>();
			}

		} catch (InvalidRequestObjectParams e) {
			ErrorDetails errorDetails = new ErrorDetails(new Date(), "Something is wrong with the request",
					"Something is wrong with the request");
			return new ResponseEntity<>(errorDetails, HttpStatus.BAD_REQUEST);
		} catch (Exception e) {
			ErrorDetails errorDetails = new ErrorDetails(new Date(), "Exception Occured while processing your request",
					"Exception Occured while processing your request");
			return new ResponseEntity<>(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<>(responseList, HttpStatus.CREATED);
	}

}
