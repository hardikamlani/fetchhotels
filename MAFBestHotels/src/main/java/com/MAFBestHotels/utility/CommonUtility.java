package com.MAFBestHotels.utility;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class CommonUtility {
	
	/**
	 * This method is used to get the date in the required format.
	 * Hardik
	 * @param inputDate
	 * @return
	 */
	public static Date getFormattedDate(String inputDate){
		Date returnValue=null;
		String pattern = CommonConstants.INPUTREQ_DATEFORMAT;
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
		try {
			returnValue = simpleDateFormat.parse(inputDate);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return returnValue;
	}

}
