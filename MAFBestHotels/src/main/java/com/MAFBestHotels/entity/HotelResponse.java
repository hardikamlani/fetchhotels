package com.MAFBestHotels.entity;

import java.util.ArrayList;
import java.util.List;

public class HotelResponse {

	private String hotelName;
	private Integer hotelRating;
	private Double hotelFare;
	private List<String> amenities;
	public String getHotelName() {
		return hotelName;
	}
	public void setHotelName(String hotelName) {
		this.hotelName = hotelName;
	}
	public Integer getHotelRating() {
		return hotelRating;
	}
	public void setHotelRating(Integer hotelRating) {
		this.hotelRating = hotelRating;
	}
	public Double getHotelFare() {
		return hotelFare;
	}
	public void setHotelFare(Double hotelFare) {
		this.hotelFare = hotelFare;
	}
	public List<String> getAmenities() {
		if(null == amenities){
			amenities = new ArrayList<String>();
		}
		return amenities;
	}
	public void setAmenities(List<String> amenities) {
		this.amenities = amenities;
	}
	
	
}
