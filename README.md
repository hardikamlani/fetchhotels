
## Technologies

1. Java 8
2. Spring Boot
3. H2 Database
4. Netflix Eureka Server,Netflix discovery client,Microservices
5. Maven 3.3
6. JUnit - Rest Assured
7. PostMan tool used to test webservices

## Steps to Run the Application

1. Run the Eureka Server 
2. Run the Crazy Hotels server
3. Run the Best Hotels server
4. Finally run the Available Hotels server

## Sample Data Inserted
Some Sample Data has been inserted in form of Sql file under resources folder of each project .
In case you are changing the test data , please make sure to run the Junit test cases and correct it.

1. Crazy Hotels Data 
insert into hotels (
       id ,
        adult_count ,
        amenities ,
        city ,
        from_date ,
        hotel_fare ,
        hotel_name,
        hotel_rating ,
        provider_name ,
        to_date ,
        discount
    )
values(10909,10,'Shower_Bath','AUH','2018-01-01','20','Avaya1',5,'BestHotels','2018-12-12',0);

2.Best Hotels Data
insert into hotels (
       id ,
        adult_count ,
        amenities ,
        city ,
        from_date ,
        hotel_fare ,
        hotel_name,
        hotel_rating ,
        provider_name ,
        to_date
    )
values(10909,10,'Shower_Bath','AUH','2018-01-01','20','Avaya1',5,'BestHotels','2018-12-12');

## MAFEurekaServer

1. Eureka Server is made with the above name to register all the microservices
2. It will run on the port - 8010 (change the port if it is occupied in your system)
3. Run this server as spring boot application

## MAFAvailableHotels

1. This server is used used to fetch the data related to the crazy hotels from the inmemory DB
2. Run this server as spring boot appication
3. It will run on the port - 8084 (change the port if it is occupied in your system)
4. Endpoint Name :-/AvailableHotels (POST Request)

Sample Req Object :-
{
 "fromDate": "2019-01-12",
 "toDate":"2019-01-20",
 "city":"auh",
 "numberOfAdults":1
}

Sample Response :- 
[   
	{
        "provider": "BestHotels",
        "hotelName": "Avaya3",
        "fare": 20,
        "amenities": [
            "Shower",
            "Bath"
        ]
    }
]

## MAFCrazyHotels

1. This server is used used to fetch the data related to the crazy hotels from the inmemory DB
2. Run this server as spring boot appication
3. It will run on the port - 8086 (change the port if it is occupied in your system)
4. Endpoint Name :-/CrazyHotels (POST Request)

Sample Req Object :-
{
 "fromDate": "2019-01-12T14:13:04.574Z",
 "toDate":"2019-01-20T14:13:04.574Z",
 "city":"auh",
 "numberOfAdults":1
}

Sample Response :- 
[   
	{
        "provider": "BestHotels",
        "hotelName": "Avaya3",
        "fare": 20,
        "amenities": [
            "Shower",
            "Bath"
        ]
    }
]

## MAFBestHotels

1. This server is used used to fetch the data related to the Best hotels from the inmemory DB
2. Run this server as spring boot appication
3. It will run on the port - 8082 (change the port if it is occupied in your system)
4. Endpoint Name :-/BestHotel (POST Request)

Sample Req Object :-
{
 "fromDate": "2019-01-12",
 "toDate":"2019-01-20",
 "city":"auh",
 "numberOfAdults":1
}

Sample Response :- 
[   
	{
        "provider": "BestHotels",
        "hotelName": "Avaya3",
        "fare": 20,
        "amenities": [
            "Shower",
            "Bath"
        ]
    }
]

## Testing

Junit Test cases have been writter in all 3 projects to test below scenarios
1. Hotelsresponse positive
2. Hotelsresponse positive zero results
3. Bad Request
4. invalid date
5. todate above fromDate
6. Invalid City.

